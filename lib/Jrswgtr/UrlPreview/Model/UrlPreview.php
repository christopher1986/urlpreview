<?php

namespace Jrswgtr\UrlPreview\Model;

/**
 * Model representing a URL preview.
 *
 * Class UrlPreview
 * @package Jrswgtr\UrlPreview\Model
 *
 * @author Joris Wagter <http://wagter.net>
 */
class UrlPreview
{
	/**
	 * @var string the URL of the preview.
	 */
	private $url;
	
	/**
	 * @var string|null the canonical URL of the page of the URL.
	 */
	private $canonicalUrl;
	
	/**
	 * @var string|null the site name of the page of the URL.
	 */
	private $siteName;
	
	/**
	 * @var string|null the title of the page of the URL.
	 */
	private $title;
	
	/**
	 * @var string|null the description of the page of the URL.
	 */
	private $description;
	
	/**
	 * @var string|null the URL to the (or an) image of the page of the URL.
	 */
	private $imageUrl;
	
	/**
	 * UrlPreview constructor.
	 *
	 * @param string $url
	 * @param string|null $canonicalUrl
	 * @param string|null $siteName
	 * @param string|null $title
	 * @param string|null $description
	 * @param string|null $imageUrl
	 */
	public function __construct(
		string $url,
		string $canonicalUrl = null,
		string $siteName = null,
		string $title = null,
		string $description = null,
		string $imageUrl = null
	) {
		$this->url          = $url;
		$this->canonicalUrl = $canonicalUrl;
		$this->siteName     = $siteName;
		$this->title        = $title;
		$this->description  = $description;
		$this->imageUrl     = $imageUrl;
	}
	
	/**
	 * Get the URL of the preview
	 *
	 * @return string the URL of the preview
	 */
	public function getUrl(): string
	{
		return $this->url;
	}
	
	/**
	 * Set the URL of the preview
	 *
	 * @param string $url the URL of the preview
	 *
	 * @return UrlPreview
	 */
	public function setUrl( string $url ): UrlPreview
	{
		$this->url = $url;
		
		return $this;
	}
	
	/**
	 * @return null|string
	 */
	public function getCanonicalUrl(): ?string
	{
		return $this->canonicalUrl;
	}
	
	/**
	 * @param null|string $canonicalUrl
	 *
	 * @return UrlPreview
	 */
	public function setCanonicalUrl( ?string $canonicalUrl ): UrlPreview
	{
		$this->canonicalUrl = $canonicalUrl;
		
		return $this;
	}
	
	/**
	 * Get the site name of the preview
	 *
	 * @return null|string
	 */
	public function getSiteName(): ?string
	{
		return $this->siteName;
	}
	
	/**
	 * Set the site name of the preview
	 *
	 * @param null|string $siteName
	 *
	 * @return UrlPreview
	 */
	public function setSiteName( ?string $siteName ): UrlPreview
	{
		$this->siteName = $siteName;
		
		return $this;
	}
	
	/**
	 * Get the title of the preview
	 *
	 * @return string the title of the preview
	 */
	public function getTitle(): ?string
	{
		return $this->title;
	}
	
	/**
	 * Set the title of the preview
	 *
	 * @param string $title the title of the preview
	 *
	 * @return UrlPreview
	 */
	public function setTitle( ?string $title ): UrlPreview
	{
		$this->title = $title;
		
		return $this;
	}
	
	/**
	 * Get the description of the preview
	 *
	 * @return string the description of the preview
	 */
	public function getDescription(): ?string
	{
		return $this->description;
	}
	
	/**
	 * Set the description of the preview
	 *
	 * @param string $description the description of the preview
	 *
	 * @return UrlPreview
	 */
	public function setDescription( ?string $description ): UrlPreview
	{
		$this->description = $description;
		
		return $this;
	}
	
	/**
	 * Get the image URL of the preview
	 *
	 * @return string the image URL of the preview
	 */
	public function getImageUrl(): ?string
	{
		return $this->imageUrl;
	}
	
	/**
	 * Set the image URL of the preview
	 *
	 * @param string $imageUrl the image URL of the preview
	 *
	 * @return UrlPreview
	 */
	public function setImageUrl( ?string $imageUrl ): UrlPreview
	{
		$this->imageUrl = $imageUrl;
		
		return $this;
	}
	
	/**
	 * Get JSON string representation of the UrlPreview
	 *
	 * @return string
	 */
	public function __toString()
	{
		$json = json_encode( [
			'url'          => $this->getUrl(),
			'canonicalUrl' => $this->getCanonicalUrl(),
			'siteName'     => $this->getSiteName(),
			'title'        => $this->getTitle(),
			'description'  => $this->getDescription(),
			'imageUrl'     => $this->getImageUrl(),
		] );
		
		return $json !== false ? $json : '';
	}
}