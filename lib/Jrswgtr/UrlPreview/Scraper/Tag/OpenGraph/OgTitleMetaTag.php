<?php

namespace Jrswgtr\UrlPreview\Scraper\Tag\OpenGraph;

use Jrswgtr\UrlPreview\Scraper\Tag\AbstractMetaTag;

/**
 * Match a og:title meta tag in a HTML document
 *
 * <meta property="og:title" content="Site Title" />
 *
 * Class OgTitleMetaTag
 * @package Jrswgtr\UrlPreview\Scraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class OgTitleMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByProperty( 'og:title', $document );
	}
}