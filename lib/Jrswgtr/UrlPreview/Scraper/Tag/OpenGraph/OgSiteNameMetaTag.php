<?php

namespace Jrswgtr\UrlPreview\Scraper\Tag\OpenGraph;

use Jrswgtr\UrlPreview\Scraper\Tag\AbstractMetaTag;

/**
 * Match a og:site_name meta tag in a HTML document
 *
 * <meta property="og:url" content="Site Name" />
 *
 * Class OgSiteNameMetaTag
 * @package Jrswgtr\UrlPreview\Scraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class OgSiteNameMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByProperty( 'og:site_name', $document );
	}
}