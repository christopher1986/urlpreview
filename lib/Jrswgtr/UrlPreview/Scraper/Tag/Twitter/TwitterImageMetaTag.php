<?php

namespace Jrswgtr\UrlPreview\Scraper\Tag\Twitter;

use Jrswgtr\UrlPreview\Scraper\Tag\AbstractMetaTag;

/**
 * Match a twitter:image meta tag in a HTML document
 *
 * <meta property="twitter:image" content="http://url-to-image.com/image.jpg" />
 *
 * Class TwitterImageMetaTag
 * @package Jrswgtr\UrlPreview\Scraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class TwitterImageMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByProperty( 'twitter:image', $document );
	}
}