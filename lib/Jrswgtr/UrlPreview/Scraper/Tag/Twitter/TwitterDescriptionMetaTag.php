<?php

namespace Jrswgtr\UrlPreview\Scraper\Tag\Twitter;

use Jrswgtr\UrlPreview\Scraper\Tag\AbstractMetaTag;

/**
 * Match a twitter:description meta tag in a HTML document
 *
 * <meta property="twitter:description" content="Site description" />
 *
 * Class TwitterDescriptionMetaTag
 * @package Jrswgtr\UrlPreview\Scraper\Tag
 *
 * @author Joris Wagter <http://wagter.net>
 */
class TwitterDescriptionMetaTag extends AbstractMetaTag
{
	/**
	 * {@inheritdoc}
	 */
	function match( string $document ): ?string
	{
		return $this->matchByProperty( 'twitter:description', $document );
	}
}