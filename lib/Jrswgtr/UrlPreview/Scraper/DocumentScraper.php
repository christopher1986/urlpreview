<?php

namespace Jrswgtr\UrlPreview\Scraper;

use Jrswgtr\UrlPreview\Scraper\Cache\CacheProviderInterface;
use Jrswgtr\UrlPreview\Scraper\Loader\DocumentLoaderInterface;
use Jrswgtr\UrlPreview\Scraper\Map\ResultMap;

class DocumentScraper implements DocumentScraperInterface
{
	/**
	 * @var DocumentLoaderInterface
	 */
	private $documentLoader;
	
	/**
	 * @var MultiScraper
	 */
	private $documentScraper;
	
	/**
	 * @var CacheProviderInterface
	 */
	private $cacheProvider;
	
	/**
	 * UrlScraper constructor.
	 *
	 * @param DocumentLoaderInterface $documentLoader
	 * @param MultiScraperInterface $documentScraper
	 * @param CacheProviderInterface $cacheProvider
	 */
	public function __construct(
		DocumentLoaderInterface $documentLoader,
		MultiScraperInterface $documentScraper,
		CacheProviderInterface $cacheProvider = null
	) {
		$this->documentLoader  = $documentLoader;
		$this->documentScraper = $documentScraper;
		$this->cacheProvider   = $cacheProvider;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function scrape( string $url ): ResultMap
	{
		$url = $this->sanitizeUrl( $url );
		
		if ( $this->cacheProvider !== null && $this->cacheProvider->has( $url ) ) {
			return $this->cacheProvider->get( $url );
		}
		
		$document = $this->documentLoader->load( $url );
		
		$resultMap = $this->documentScraper->scrape( $document );
		
		$resultMap->put( 'url', $url );
		
		if ( $this->cacheProvider !== null ) {
			$this->cacheProvider->put( $url, $resultMap );
		}
		
		return $resultMap;
	}
	
	/**
	 * Sanitize a URL
	 *
	 * @param string $url the URL to sanitize
	 *
	 * @return string the sanitized URL
	 */
	private function sanitizeUrl( string $url ): string
	{
		return htmlspecialchars( trim( $url ), ENT_QUOTES, 'ISO-8859-1', true );
	}
}