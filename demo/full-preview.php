<?php
/**
 * A demo showing a HTML URL preview including title, description and image.
 */

use Jrswgtr\UrlPreview\Model\UrlPreview;
use Jrswgtr\UrlPreview\Scraper\Cache\FileSystemCacheProvider;
use Jrswgtr\UrlPreview\Scraper\DocumentScraper;
use Jrswgtr\UrlPreview\Scraper\Loader\DummyDocumentLoader;
use Jrswgtr\UrlPreview\Scraper\MultiScraper;
use Jrswgtr\UrlPreview\Scraper\ResultStuffer;
use Jrswgtr\UrlPreview\Scraper\SingleScraper;
use Jrswgtr\UrlPreview\Scraper\Tag\DescriptionMetaTag;
use Jrswgtr\UrlPreview\Scraper\Tag\ImageTag;
use Jrswgtr\UrlPreview\Scraper\Tag\OpenGraph\OgDescriptionMetaTag;
use Jrswgtr\UrlPreview\Scraper\Tag\OpenGraph\OgImageMetaTag;
use Jrswgtr\UrlPreview\Scraper\Tag\OpenGraph\OgTitleMetaTag;
use Jrswgtr\UrlPreview\Scraper\Tag\ParagraphTag;
use Jrswgtr\UrlPreview\Scraper\Tag\TitleTag;
use Jrswgtr\UrlPreview\Scraper\Tag\Twitter\TwitterDescriptionMetaTag;
use Jrswgtr\UrlPreview\Scraper\Tag\Twitter\TwitterImageMetaTag;
use Jrswgtr\UrlPreview\Scraper\Tag\Twitter\TwitterTitleMetaTag;

require_once '../vendor/autoload.php';

$documentLoader = new DummyDocumentLoader();

$titleScraper = new SingleScraper( [
	new OgTitleMetaTag(),
	new TwitterTitleMetaTag(),
	new TitleTag(),
] );

$descriptionScraper = new SingleScraper( [
	new DescriptionMetaTag(),
	new OgDescriptionMetaTag(),
	new TwitterDescriptionMetaTag(),
	new ParagraphTag(),
] );

$imageUrlScraper = new SingleScraper( [
	new OgImageMetaTag(),
	new TwitterImageMetaTag(),
	new ImageTag(),
] );

$multiScraper = new MultiScraper();

$multiScraper
	->setScraper( 'title', $titleScraper )
	->setScraper( 'description', $descriptionScraper )
	->setScraper( 'imageUrl', $imageUrlScraper )
;

$cacheProvider = new FileSystemCacheProvider(
	$path = dirname( __FILE__, 2 ) . '/var/cache',
	$lifeTime = 172800 // 48 * 60 * 60
);

$docScraper = new DocumentScraper( $documentLoader, $multiScraper, $cacheProvider );


$url = 'http://dummy-url-test.com';

$results = $docScraper->scrape( $url );

$urlPreview    = new UrlPreview( $url );
$resultStuffer = new ResultStuffer();

$resultStuffer->stuff( $results, $urlPreview );

?>
<html>
<head>

    <title>Jrswgtr UrlPreview demo - Full preview</title>

    <link href="./css/demo.css" rel="stylesheet" type="text/css"/>

</head>
<body>

<div class="wrapper">
    <h1>Jrswgtr UrlPreview demo - Full preview</h1>
	<?php if ( $urlPreview !== null ): ?>
        <a href="<?= $urlPreview->getUrl(); ?>" target="_blank" class="url-preview">
			
			<?php if ( $urlPreview->getImageUrl() !== null ): ?>
                <img src="<?= $urlPreview->getImageUrl(); ?>" class="img"/>
			<?php endif; ?>
			
			<?php if ( $urlPreview->getTitle() !== null ): ?>
                <h3><?= $urlPreview->getTitle(); ?></h3>
			<?php endif; ?>
			
			<?php if ( $urlPreview->getDescription() !== null ): ?>
                <p><?= $urlPreview->getDescription(); ?></p>
			<?php endif; ?>

            <p><?= $urlPreview->getUrl(); ?></p>

        </a>
	<?php endif; ?>
</div>

</body>
</html>