# UrlPreview
A small PHP library to scrape URL preview data.

Including

* title
* description
* image url
 
## Installation
To add to your existing composer project

`composer require jrswgtr/urlpreview`

To edit the package

`git clone git@bitbucket.org:jrswgtr/urlpreview.git`

## Usage

For usage examples, see the [demo directory](https://bitbucket.org/jrswgtr/urlpreview/src/master/demo/).

### Run the demo's

clone the project

`git clone git@bitbucket.org:jrswgtr/urlpreview.git`

enter the directory

`cd urlpreview`

install project

`composer install`

start web server

`php -S localhost:8000 -t demo/`

Now you can run the demo's at:

* [http://localhost:8000/full-preview.php](http://localhost:8000/full-preview.php)
* [http://localhost:8000/full-preview-ajax.php](http://localhost:8000/full-preview-ajax.php)
